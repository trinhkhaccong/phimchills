from selenium.webdriver.chrome.options import Options
import time
import mysql.connector
from unidecode import unidecode
from seleniumwire import webdriver  

connection  = mysql.connector.connect(
    host='103.200.23.160',
    database='doithec4_phimchills',
    user='doithec4_phimchills',
    password='Hunter2016@1995ctk'
)
if connection.is_connected():
    db_Info = connection.get_server_info()
    print("Connected to MySQL Server version ", db_Info)

cursor = connection.cursor()

def check_the_loai(v_string):
    list_link_theloai=[]
    list_theloai=[]
    if(v_string.find("Đam Mỹ - Bách Hợp")>-1):
        list_link_theloai.append("dam-my-bach-hop")
        list_theloai.append("Đam Mỹ - Bách Hợp")

    if(v_string.find("Phim Chill")>-1):
        list_link_theloai.append("phim-chill")
        list_theloai.append("Phim Chill")

    if(v_string.find("Hành Động")>-1):
        list_link_theloai.append("hanh-dong")
        list_theloai.append("Hành Động")

    if(v_string.find("Võ Thuật - Kiếm Hiệp")>-1):
        list_link_theloai.append("vo-thuat-kiem-hiep")
        list_theloai.append("Võ Thuật - Kiếm Hiệp")

    if(v_string.find("Tâm Lý - Tình Cảm")>-1):
        list_link_theloai.append("tam-ly-tinh-cam")
        list_theloai.append("Tâm Lý - Tình Cảm")

    if(v_string.find("Hài Hước")>-1):
        list_link_theloai.append("hai-huoc")
        list_theloai.append("Hài Hước")

    if(v_string.find("Hoạt Hình")>-1):
        list_link_theloai.append("anime-hoat-hinh")
        list_theloai.append("Anime - Hoạt Hình")

    if(v_string.find("Viễn Tưởng")>-1):
        list_link_theloai.append("vien-tuong")
        list_theloai.append("Viễn Tưởng")

    if(v_string.find("Hình Sự")>-1):
        list_link_theloai.append("hinh-su")
        list_theloai.append("Hình Sự")

    if(v_string.find("Kinh Dị")>-1):
        list_link_theloai.append("kinh-di")
        list_theloai.append("Kinh Dị")

    if(v_string.find("Chiến Tranh")>-1):
        list_link_theloai.append("chien-tranh")
        list_theloai.append("Chiến Tranh")

    if(v_string.find("Phiêu Lưu")>-1):
        list_link_theloai.append("phieu-luu")
        list_theloai.append("Phiêu Lưu")

    if(v_string.find("Bí Ẩn")>-1):
        list_link_theloai.append("bi-an")
        list_theloai.append("Bí Ẩn")

    if(v_string.find("Khoa Học")>-1):
        list_link_theloai.append("khoa-hoc")
        list_theloai.append("Khoa Học")

    if(v_string.find("Gia Đình")>-1):
        list_link_theloai.append("gia-dinh")
        list_theloai.append("Gia Đình")

    if(v_string.find("Cao Bồi")>-1):
        list_link_theloai.append("cao-boi")
        list_theloai.append("Cao Bồi")
   
    if(v_string.find("Âm Nhạc")>-1):
        list_link_theloai.append("am-nhac")
        list_theloai.append("Âm Nhạc")

    if(v_string.find("Thể Thao")>-1):
        list_link_theloai.append("the-thao")
        list_theloai.append("Thể Thao")

    if(v_string.find("Truyền Hình")>-1):
        list_link_theloai.append("truyen-hinh")
        list_theloai.append("Truyền Hình")

    if(v_string.find("TV Show")>-1):
        list_link_theloai.append("tv-show")
        list_theloai.append("TV Show")
        
    if(v_string.find("Lịch Sử")>-1):
        list_link_theloai.append("lich-su")
        list_theloai.append("Lịch Sử")

    if(v_string.find("Tài Liệu")>-1):
        list_link_theloai.append("tai-lieu")
        list_theloai.append("Tài Liệu")

    if(v_string.find("Xuyên Không")>-1):
        list_link_theloai.append("xuyen-khong")
        list_theloai.append("Xuyên Không")

    if(v_string.find("Cổ Trang")>-1):
        list_link_theloai.append("co-trang")
        list_theloai.append("Cổ Trang")

    if(v_string.find("Học Đường")>-1):
        list_link_theloai.append("hoc-duong")
        list_theloai.append("Học Đường")

    if(v_string.find("Y Khoa Bác Sĩ")>-1):
        list_link_theloai.append("y-khoa-bac-si")
        list_theloai.append("Y Khoa Bác Sĩ")

    if(v_string.find("Khác")>-1):
        list_link_theloai.append("khac")
        list_theloai.append("Khác")

    return " ,".join(list_link_theloai)," ,".join(list_theloai)

 

def check_country(v_string):
    if(v_string.find("Ấn Độ")>-1):
        return 1 
    if(v_string.find("Anh")>-1):
        return 2
    if(v_string.find("Canada")>-1):
        return 3
    if(v_string.find("Châu Âu")>-1):
        return 4
    if(v_string.find("Đài Loan")>-1):
        return 5
    if(v_string.find("Đức")>-1):
        return 6
    if(v_string.find("Hàn Quốc")>-1):
        return 7
    if(v_string.find("Hồng Kông")>-1):
        return 8
    if(v_string.find("Mỹ")>-1):
        return 9
    if(v_string.find("Nga")>-1):
        return 10
    if(v_string.find("Nhật Bản")==0):
        return 11
    if(v_string.find("Pháp")>-1):
        return 12
    if(v_string.find("Tây Ban Nha")>-1):
        return 13
    if(v_string.find("Thái Lan")>-1):
        return 14
    if(v_string.find("Trung Quốc")>-1):
        return 15
    if(v_string.find("Úc")>-1):
        return 16
    if(v_string.find("Việt Nam")>-1):
        return 17
    if(v_string.find("Ý")>-1):
        return 18
    else:
        return 19
    

if __name__ == "__main__":
    lines_phim_le=[]
    with open("phim-bo","r",encoding="utf8") as tf:
        lines_phim_le= tf.readlines()
    with open("phim-bo-backup","r+",encoding="utf8") as tf1:
        lines_phim_le_bk= tf1.readlines()
    options = webdriver.ChromeOptions()
    # Working with the 'add_argument' Method to modify Driver Default Notification
    options.add_argument('--disable-notifications')
    
    for link in lines_phim_le:
        try:
            if link in lines_phim_le_bk:
                continue
            driver = webdriver.Chrome(chrome_options=options)
            driver.maximize_window()
            _link_background=""
            _root_link=""
            _ep_today =""
            _name_vn=""
            _name_en=""
            _content=""
            _year=""
            _director=""
            _cast=""
            _link=""
            _id_typephim=2
            _id_quocgia=""
            _list_theloai=""
            _list_link_theloai=""
            _time=""
            _timestamp = int(time.time())
            driver.get(link.replace('\n',""))
            try:
                body=driver.find_element_by_id("page-info")
            except:
                continue
            info = body.find_element_by_class_name("poster")
            _link_background= info.find_element_by_tag_name("img").get_attribute("src")
            try:
                _root_link=info.find_element_by_class_name("tab-wrapper-info").find_element_by_class_name("btn-warning").get_attribute("href")
            except:
                continue
            _name_vn = body.find_element_by_tag_name("h1").text.title()
            _name_en =body.find_element_by_class_name("py-2").text
            _link=unidecode(_name_vn).replace(" ","-").replace(":","").lower()
            
            info=body.find_element_by_class_name("meta-data").text.replace(":\n",":")
            list_info = info.split("\n")
            for ltinfo in list_info:
                if(ltinfo.find("Đạo diễn:")>-1):
                    _director=ltinfo.replace("Đạo diễn:","")
                if(ltinfo.find("Quốc gia:")>-1):
                    _id_quocgia= check_country(ltinfo.replace("Quốc gia:","").title())
                if(ltinfo.find("Thời lượng:")>-1):
                    _time=ltinfo.replace("Thời lượng:","")
                if(ltinfo.find("Thể loại:")>-1):
                    _list_link_theloai,_list_theloai=check_the_loai(ltinfo.replace("Thể loại:","").title())
                if(ltinfo.find("Diễn viên:")>-1):
                    _cast=ltinfo.replace("Diễn viên:","")
                if(ltinfo.find("Năm sản xuất:")>-1):
                    _year=ltinfo.replace("Năm sản xuất:","")
                if(ltinfo.find("Trạng thái:")>-1):
                    _ep_today=ltinfo.replace("Trạng thái:","")
            _content = driver.find_element_by_class_name("content-phim").text.replace("Nội dung phim","")
            check_insert = ("SELECT * FROM tb_filmintro " "WHERE link=%s")
            insert = ("INSERT INTO tb_filmintro (year, director, cast, content, link,link_background,name_vn,name_en,id_typephim,id_quocgia,list_theloai,list_link_theloai,time,timestamp,root_link,ep_today) VALUES (%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s,%s, %s, %s, %s, %s)")
            data = (_year, _director, _cast, _content, _link,_link_background,_name_vn,_name_en,_id_typephim,_id_quocgia,_list_theloai,_list_link_theloai,_time,_timestamp,_root_link,_ep_today,)
            cursor.execute(check_insert,(_link,))
            if(len(cursor.fetchall()) ==0):
                #insert
                cursor.execute(insert, data)
                connection.commit()
                time.sleep(2)
            else:
                reset = ("UPDATE tb_filmintro set ep_today =%s WHERE link=%s")
                cursor.execute(reset,(_ep_today,_link,))
                connection.commit()
                time.sleep(2)
            #tb_theloai_info
            with open("phim-bo-backup","a+",encoding="utf8") as tf2:
                tf2.write(link)
            #download link video
            driver.get(_root_link)
            # driver.find_element_by_class_name("ads").click()
            list_elements = driver.find_elements_by_class_name("list-episode")
            list_li=[]
            for elements in list_elements:
                try:
                    list_li=elements.find_elements_by_tag_name("a")
                    if len(list_li)>0:
                        break
                except:
                    continue
            list_url =[]
            list_name=[]
            for element in list_li:
                list_name.append(unidecode(element.text))
                list_url.append(element.get_attribute("href"))
            driver.quit()
            for i in range(len(list_url)):
                check_insert = ("SELECT root_link FROM tb_film " "WHERE root_link=%s AND episode=%s")
                cursor.execute(check_insert,(_link,list_name[i],))
                if(len(cursor.fetchall()) ==0):
                    driver = webdriver.Chrome(options=options)
                    driver.maximize_window()
                    link_film=""
                    name_vn=_name_vn
                    name_en=_name_en
                    link_video_tm="https://tiengdong.com/wp-content/uploads/Video-a-few-moments-later-www_tiengdong_com.mp4?_=1"
                    link_video_sub="https://tiengdong.com/wp-content/uploads/Video-a-few-moments-later-www_tiengdong_com.mp4?_=1"
                    episode=list_name[i]
                    root_link=_link
                    id_link=""
                    link_film=list_url[i]
                    print(link_film+"\n")
                    # driver.find_element_by_class_name("ads").click()
                    try:
                        driver.get(link_film)
                        time.sleep(2)
                        driver.find_element_by_class_name("jw-icon-display").click()
                    except:
                        print("khong phải click")
                    time.sleep(5)
                    check= False
                    for request in driver.requests:
                        if request.response:
                            if(request.url.find(".ts")>0):
                                print(request.url)
                            if(request.url.find(".m3u8")>0):
                                link_video_sub=request.url
                                link_video_tm=request.url
                                check=True
                            if(request.url.find("cdninstagram.com")>0):
                                link_video_sub=request.url
                                link_video_tm=request.url
                                check=True
                    if check== False:
                        time.sleep(10)
                        for request in driver.requests:
                            if request.response:
                                if(request.url.find(".ts")>0):
                                    print(request.url)
                                if(request.url.find(".m3u8")>0):
                                    link_video_sub=request.url
                                    link_video_tm=request.url
                                    check=True
                                if(request.url.find("cdninstagram.com")>0):
                                    link_video_sub=request.url
                                    link_video_tm=request.url
                                    check=True
                    if check:
                        insert = ("INSERT INTO tb_film(name_vn, name_en, link_video_tm, link_video_sub, episode,root_link,id_link,is_videos) VALUES (%s, %s, %s, %s, %s,%s, %s,%s)")
                        data = (name_vn,name_en,link_video_tm,link_video_sub,episode,root_link,link_film,1,)
                        cursor.execute(insert,data)
                        connection.commit()
                    else:
                        insert = ("INSERT INTO tb_film(name_vn, name_en, link_video_tm, link_video_sub, episode,root_link,id_link,is_videos) VALUES (%s, %s, %s, %s, %s,%s, %s,%s)")
                        data = (name_vn,name_en,link_video_tm,link_video_sub,episode,root_link,link_film,0,)
                        cursor.execute(insert,data)
                        connection.commit()
                    driver.quit()
        except:
            continue

            
        # list_url=body.find_elements_by_class_name("item")
        # for item in list_url:
        #     link = item.find_element_by_tag_name("a").get_attribute("href")+"\n"
        #     if link not in lines_phim_le:
    