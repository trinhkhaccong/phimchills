from seleniumwire import webdriver
from selenium.webdriver.chrome.options import Options
import time
import mysql.connector
from unidecode import unidecode

connection  = mysql.connector.connect(
    host='103.200.23.160',
    database='doithec4_phimchills',
    user='doithec4_phimchills',
    password='Hunter2016@1995ctk'
)
if connection.is_connected():
    db_Info = connection.get_server_info()
    print("Connected to MySQL Server version ", db_Info)

cursor = connection.cursor()

def check_the_loai(v_string):
    list_link_theloai=[]
    list_theloai=[]
    if(v_string.find("Phim Hình Sự")>-1):
        list_link_theloai.append("phim-hinh-su")
        list_theloai.append("Phim Hình Sự")

    if(v_string.find("Phim Ma - Kinh Dị")>-1):
        list_link_theloai.append("phim-ma-kinh-di")
        list_theloai.append("Phim Ma - Kinh Dị")

    if(v_string.find("Phim Thần Thoại")>-1):
        list_link_theloai.append("phim-than-thoai")
        list_theloai.append("Phim Thần Thoại")

    if(v_string.find("Phim Gia Đình")>-1):
        list_link_theloai.append("phim-gia-dinh")
        list_theloai.append("Phim Gia Đình")

    if(v_string.find("Phim Hành Động")>-1):
        list_link_theloai.append("phim-hanh-dong")
        list_theloai.append("Phim Hành Động")

    if(v_string.find("Phim Võ Thuật")>-1):
        list_link_theloai.append("phim-vo-thuat")
        list_theloai.append("Phim Võ Thuật")

    if(v_string.find("Phim Tình Cảm")>-1):
        list_link_theloai.append("phim-tinh-cam")
        list_theloai.append("Phim Tình Cảm")

    if(v_string.find("Phim Chiến Tranh")>-1):
        list_link_theloai.append("phim-chien-tranh")
        list_theloai.append("Phim Chiến Tranh")

    if(v_string.find("Phim Viễn Tưởng")>-1):
        list_link_theloai.append("phim-vien-tuong")
        list_theloai.append("Phim Viễn Tưởng")

    if(v_string.find("Phim Hài Hước")>-1):
        list_link_theloai.append("phim-hai-huoc")
        list_theloai.append("Phim Hài Hước")

    if(v_string.find("Phim Cổ Trang")>-1):
        list_link_theloai.append("phim-co-trang")
        list_theloai.append("Phim Cổ Trang")

    if(v_string.find("Phim Phiêu Lưu")>-1):
        list_link_theloai.append("phim-phieu-luu")
        list_theloai.append("Phim Phiêu Lưu")

    if(v_string.find("Phim Âm Nhạc")>-1):
        list_link_theloai.append("phim-am-nhac")
        list_theloai.append("Phim Âm Nhạc")

    if(v_string.find("Phim Thể Thao")>-1):
        list_link_theloai.append("phim-the-thao")
        list_theloai.append("Phim Thể Thao")

    if(v_string.find("Phim LGBT")>-1):
        list_link_theloai.append("phim-lgbt")
        list_theloai.append("Phim LGBT")

    if(v_string.find("Phim Học Đường")>-1):
        list_link_theloai.append("phim-hoc-duong")
        list_theloai.append("Phim Học Đường")
   
    if(v_string.find("Phim Anime")>-1):
        list_link_theloai.append("phim-anime")
        list_theloai.append("Phim Anime")

    if(v_string.find("Phim Chính Kịch")>-1):
        list_link_theloai.append("phim-chinh-kich")
        list_theloai.append("Phim Chính Kịch")

    return " ,".join(list_link_theloai)," ,".join(list_theloai)

 

def check_country(v_string):
    if(v_string.find("Phim Hàn Quốc")>-1):
        return 1 
    if(v_string.find("Phim Mỹ")>-1):
        return 2
    if(v_string.find("Phim Trung Quốc")>-1):
        return 3
    if(v_string.find("Phim Thái Lan")>-1):
        return 4
    if(v_string.find("Phim Châu Âu")>-1):
        return 5
    if(v_string.find("Phim Châu Á")>-1):
        return 6
    if(v_string.find("Phim Nhật Bản")>-1):
        return 7
    else:
        return 6
    

if __name__ == "__main__":
    lines_phim_le=[]
    with open("phim-le","r",encoding="utf8") as tf:
        lines_phim_le= tf.readlines()
    with open("phim-le-backup","r+",encoding="utf8") as tf1:
        lines_phim_le_bk= tf1.readlines()
    with open("phim-chieu-rap","r+",encoding="utf8") as tf1:
        lines_phim_chieu_rap= tf1.readlines()
    options = Options()
    # Working with the 'add_argument' Method to modify Driver Default Notification
    options.add_argument('--disable-notifications')
    
    for link in lines_phim_le:
        try:
            _link_background=""
            _root_link=""
            _ep_today =""
            _name_vn=""
            _name_en=""
            _content=""
            _year=""
            _director=""
            _cast=""
            _link=""
            _id_typephim=1
            _id_quocgia=""
            _list_theloai=""
            _list_link_theloai=""
            _time=""
            if link in lines_phim_le_bk:
                continue

            if link in lines_phim_chieu_rap:
                _list_theloai="Phim Chiếu Rạp, "
                _list_link_theloai="phim-chieu-rap, "
            driver = webdriver.Chrome(chrome_options=options)
            driver.maximize_window()
            
            _timestamp = int(time.time())
            driver.get(link.replace('\n',""))
            try:
                body=driver.find_element_by_class_name("blockbody").find_element_by_class_name("info")
            except:
                continue
            info = body.find_element_by_class_name("poster")
            _link_background= info.find_element_by_tag_name("img").get_attribute("src")
            try:
                _root_link=info.find_element_by_class_name("two-button").find_element_by_class_name("btn-danger").get_attribute("href")
            except:
                continue
            _name_vn = body.find_element_by_tag_name("h1").text.title()
            _name_en =body.find_element_by_class_name("real-name").text
            _link=unidecode(_name_vn).replace(" ","-").replace(":","").replace(",","").replace("&","").replace("(","").replace(")","").lower()
            
            info=body.find_element_by_class_name("dinfo").text.replace(":\n",":")
            list_info = info.split("\n")
            for ltinfo in list_info:
                if(ltinfo.find("Đạo diễn:")>-1):
                    _director=ltinfo.replace("Đạo diễn:","")
                if(ltinfo.find("Quốc gia:")>-1):
                    _id_quocgia= check_country(ltinfo.replace("Quốc gia:","").title())
                if(ltinfo.find("Thời lượng:")>-1):
                    _time=ltinfo.replace("Thời lượng:","")
                if(ltinfo.find("Thể loại:")>-1):
                    link_theloai,theloai=check_the_loai(ltinfo.replace("Thể loại:","").title())
                    _list_theloai=_list_theloai+theloai
                    _list_link_theloai=_list_link_theloai+link_theloai
                if(ltinfo.find("Diễn viên:")>-1):
                    _cast=ltinfo.replace("Diễn viên:","")
                if(ltinfo.find("Năm sản xuất:")>-1):
                    _year=ltinfo.replace("Năm sản xuất:","")
                if(ltinfo.find("Trạng thái:")>-1):
                    _ep_today=ltinfo.replace("Trạng thái:","")
            _content ="PhimChills - Phim mới nhất cập nhật trong ngày - "+ driver.find_element_by_class_name("tabs-content").text.replace("NỘI DUNG PHIM\n","")
            check_insert = ("SELECT * FROM tb_filmintro " "WHERE link=%s")
            insert = ("INSERT INTO tb_filmintro (year, director, cast, content, link,link_background,name_vn,name_en,id_typephim,id_quocgia,list_theloai,list_link_theloai,time,timestamp,root_link,ep_today) VALUES (%s, %s, %s, %s, %s,%s, %s, %s, %s, %s,%s,%s, %s, %s, %s, %s)")
            data = (_year, _director, _cast, _content, _link,_link_background,_name_vn,_name_en,_id_typephim,_id_quocgia,_list_theloai,_list_link_theloai,_time,_timestamp,_root_link,_ep_today,)
            cursor.execute(check_insert,(_link,))
            if(len(cursor.fetchall()) ==0):
                #insert
                cursor.execute(insert, data)
                connection.commit()
                time.sleep(2)
            else:
                reset = ("UPDATE tb_filmintro set ep_today =%s WHERE link=%s")
                cursor.execute(reset,(_ep_today,_link,))
                connection.commit()
                time.sleep(2)
            #tb_theloai_info
            with open("phim-le-backup","a+",encoding="utf8") as tf2:
                tf2.write(link)
            #download link video
            link_cawl = _root_link
            check_insert = ("SELECT episode,root_link FROM tb_film where root_link=%s")
            cursor.execute(check_insert,(_link,))
            if(len(cursor.fetchall()) ==0):
                link_film=""
                name_vn=_name_vn
                name_en=_name_en
                link_video_tm="https://tiengdong.com/wp-content/uploads/Video-a-few-moments-later-www_tiengdong_com.mp4?_=1"
                link_video_sub="https://tiengdong.com/wp-content/uploads/Video-a-few-moments-later-www_tiengdong_com.mp4?_=1"
                episode="full"
                root_link=_link
                id_link=""
                link_film=link_cawl
                print(link_cawl+"\n")
                driver.get(link_cawl)
                # driver.find_element_by_class_name("ads").click()
                time.sleep(2)
                try:
                    driver.find_element_by_class_name("jw-icon-display").click()
                except:
                    print("khong phải click")
                time.sleep(5)
                check= False
                for request in driver.requests:
                    if request.response:
                        if(request.url.find(".gif")>0):
                            continue
                        if(request.url.find(".ts")>0):
                            print(request.url)
                        if(request.url.find(".m3u8")>0):
                            link_video_sub=request.url
                            link_video_tm=request.url
                            check=True
                        if(request.url.find("cdninstagram.com")>0):
                            link_video_sub=request.url
                            link_video_tm=request.url
                            check=True
                if check== False:
                    time.sleep(5)
                    for request in driver.requests:
                        if request.response:
                            if(request.url.find(".gif")>0):
                                continue
                            if(request.url.find(".ts")>0):
                                print(request.url)
                            if(request.url.find(".m3u8")>0):
                                link_video_sub=request.url
                                link_video_tm=request.url
                                check=True
                            if(request.url.find("cdninstagram.com")>0):
                                link_video_sub=request.url
                                link_video_tm=request.url
                                check=True
                if check:
                    insert = ("INSERT INTO tb_film(name_vn, name_en, link_video_tm, link_video_sub, episode,root_link,id_link,is_videos) VALUES (%s, %s, %s, %s, %s,%s, %s,%s)")
                    data = (name_vn,name_en,link_video_tm,link_video_sub,episode,root_link,link_film,1)
                    cursor.execute(insert,data)
                    connection.commit()
                else:
                    insert = ("INSERT INTO tb_film(name_vn, name_en, link_video_tm, link_video_sub, episode,root_link,id_link,is_videos) VALUES (%s, %s, %s, %s, %s,%s, %s,%s)")
                    data = (name_vn,name_en,link_video_tm,link_video_sub,episode,root_link,link_film,0)
                    cursor.execute(insert,data)
                    connection.commit()
            driver.quit()
        except Exception as ex:
            print(ex)
            driver.quit()
            continue

            
        # list_url=body.find_elements_by_class_name("item")
        # for item in list_url:
        #     link = item.find_element_by_tag_name("a").get_attribute("href")+"\n"
        #     if link not in lines_phim_le:
    