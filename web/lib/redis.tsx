import Redis from 'ioredis'
const redis = new Redis();
import {Md5} from 'ts-md5';

export const SetRedis=async(key:any,value:any)=>
{
    try {
      let time:any = new Date()
        let time_cv:any = time/1000
        await redis.set(Md5.hashStr(key),value)
        redis.expireat(Md5.hashStr(key), parseInt(time_cv) + 60*60*2);
      } catch (error) {
        return { error };
      }
}

export const GetRedis=async(key:any)=>
{
    try {
        let data:any = await redis.get(Md5.hashStr(key))
        return  JSON.parse(data)
      } catch (error) {
        return { error };
      }
}
