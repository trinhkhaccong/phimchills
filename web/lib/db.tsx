import mysql from 'serverless-mysql';

interface excuteQueryProps
{
    query:any;
    values:any;
}

const db = mysql({
  config: {
    host:'103.200.23.160',
    database:'doithec4_phimchills',
    user:'doithec4_phimchills',
    password:'Hunter2016@1995ctk'
  }
});

export async function excuteQuery(props:excuteQueryProps) {
  try {
    const {query,values} = props
    const results = await db.query(query, values);
    await db.end();
    return results;
  } catch (error) {
    return { error };
  }
}

export async function excuteQueryOb(props:excuteQueryProps) {
  try {
    let results:any=[]
    const {query,values} = props
    results = await db.query(query, values);
    await db.end();
    return results[0];
  } catch (error) {
    return { error };
  }
}