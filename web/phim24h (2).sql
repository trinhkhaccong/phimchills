-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 18, 2022 lúc 04:16 PM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `phim24h`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_film`
--

CREATE TABLE `tb_film` (
  `id` int(11) NOT NULL,
  `name_vn` text NOT NULL,
  `name_en` text NOT NULL,
  `link_video_tm` text NOT NULL,
  `link_video_sub` text NOT NULL,
  `episode` int(11) NOT NULL,
  `root_link` text NOT NULL,
  `id_link` text NOT NULL,
   `is_videos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_filmintro`
--

CREATE TABLE `tb_filmintro` (
  `id` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `director` text NOT NULL,
  `cast` text NOT NULL,
  `content` text NOT NULL,
  `link` text NOT NULL,
  `link_background` text NOT NULL,
  `name_vn` text NOT NULL,
  `name_en` text NOT NULL,
  `id_typephim` int(11) NOT NULL,
  `id_quocgia` int(11) NOT NULL,
  `list_theloai` text NOT NULL,
  `list_link_theloai` text NOT NULL,
  `time` text NOT NULL,
  `timestamp` int(11) NOT NULL,
  `root_link` text NOT NULL,
  `ep_today` text NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_quocgia`
--

CREATE TABLE `tb_quocgia` (
  `id` int(11) NOT NULL,
  `link` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_quocgia`
--

INSERT INTO `tb_quocgia` (`id`, `link`, `name`) VALUES
(1, 'an-do', 'Ấn Độ'),
(2, 'anh', 'Anh'),
(3, 'canada', 'Canada'),
(4, 'chau-au', 'Châu Âu'),
(5, 'dai-loan', 'Đài Loan'),
(6, 'duc', 'Đức'),
(7, 'han-quoc', 'Hàn Quốc'),
(8, 'hong-kong', 'Hồng Kông'),
(9, 'my', 'Mỹ'),
(10, 'nga', 'Nga'),
(11, 'nhat-ban', 'Nhật Bản'),
(12, 'phap', 'Pháp'),
(13, 'tay-ban-nha', 'Tây Ban Nha'),
(14, 'thai-lan', 'Thái Lan'),
(15, 'trung-quoc', 'Trung Quốc'),
(16, 'uc', 'Úc'),
(17, 'viet-nam', 'Việt Nam'),
(18, 'ý', 'Ý'),
(19, 'nuoc-khac', 'Nước Khác');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_theloai`
--

CREATE TABLE `tb_theloai` (
  `id` int(11) NOT NULL,
  `link` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_theloai`
--

INSERT INTO `tb_theloai` (`id`, `link`, `name`) VALUES
(1, 'dam-my-bach-hop', 'Đam Mỹ - Bách Hợp'),
(2, 'phim-chill', 'Phim Chill'),
(3, 'hanh-dong', 'Hành Động'),
(4, 'vo-thuat-kiem-hiep', 'Võ Thuật - Kiếm Hiệp'),
(5, 'tam-ly-tinh-cam', 'Tâm Lý - Tình Cảm'),
(6, 'kinh-di', 'Kinh Dị'),
(7, 'hai-huoc', 'Hài Hước'),
(8, 'anime-hoat-hinh', 'Anime - Hoạt Hình'),
(9, 'vien-tuong', 'Viễn Tưởng'),
(10, 'hinh-su', 'Hình Sự'),
(11, 'chien-tranh', 'Chiến Tranh'),
(12, 'phieu-luu', 'Phiêu Lưu'),
(13, 'bi-an', 'Bí Ẩn'),
(14, 'khoa-hoc', 'Khoa Học'),
(15, 'gia-dinh', 'Gia Đình'),
(16, 'cao-boi', 'Cao Bồi'),
(17, 'am-nhac', 'Âm Nhạc'),
(18, 'the-thao', 'Thể Thao'),
(19, 'truyen-hinh', 'Truyền Hình'),
(20, 'tv-show', 'TV Show'),
(21, 'lich-su', 'Lịch Sử'),
(22, 'tai-lieu', 'Tài liệu'),
(23, 'xuyen-khong', 'Xuyên Không'),
(24, 'co-trang', 'Cổ Trang'),
(25, 'hoc-duong', 'Học Đường'),
(26, 'y-khoa-bac-si', 'Y Khoa Bác Sĩ'),
(27, 'khac', 'Khác');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_typephim`
--

CREATE TABLE `tb_typephim` (
  `id` int(11) NOT NULL,
  `link` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tb_typephim`
--

INSERT INTO `tb_typephim` (`id`, `link`, `name`) VALUES
(1, 'phim-le', 'Phim Lẻ'),
(2, 'phim-bo', 'Phim Bộ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tb_view`
--

CREATE TABLE `tb_view` (
  `id` int(11) NOT NULL,
  `root_link` text NOT NULL,
  `date` date NOT NULL,
  `views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tb_film`
--
ALTER TABLE `tb_film`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_filmintro`
--
ALTER TABLE `tb_filmintro`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_quocgia`
--
ALTER TABLE `tb_quocgia`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_theloai`
--
ALTER TABLE `tb_theloai`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_typephim`
--
ALTER TABLE `tb_typephim`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tb_view`
--
ALTER TABLE `tb_view`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tb_film`
--
ALTER TABLE `tb_film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT cho bảng `tb_filmintro`
--
ALTER TABLE `tb_filmintro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT cho bảng `tb_quocgia`
--
ALTER TABLE `tb_quocgia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT cho bảng `tb_theloai`
--
ALTER TABLE `tb_theloai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT cho bảng `tb_typephim`
--
ALTER TABLE `tb_typephim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT cho bảng `tb_view`
--
ALTER TABLE `tb_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
