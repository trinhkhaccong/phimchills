import Head from 'next/head';
import { useRouter } from 'next/router';
import * as React from 'react';
import { json } from 'stream/consumers';
import UiCome from '../../component/Ui/UiCome';
import UiHead from '../../component/Ui/UiHead';
import styles from "../../styles/Home.module.scss"
import UiTrending from '../../component/Ui/UiTrending';
import UiFooter from '../../component/Ui/UiFooter';
import UiHomeFilm from '../../component/Ui/UiHomeFilm';
import UiInfoFilm from '../../component/Ui/UiInfoFilm';
import {APIGetCarousel,APIGetIntroLink,GetIntroTheLoaiProp,APIGetIntroTheLoai,APIGetUpdateView,APIGetTrend,APIGetTheLoai,APIGetCountry} from "../../component/Func/APIGet"
export interface InfoIDPostProps {
  data_all:any;
  data:any;
  date_now:any;
  date_week:any;
  date_month:any;
  data_theloai:any;
  data_country:any;
  path:any
}

export interface IntroLinkProp
{
    link:any;
}

export default function InfoIDPost(props: InfoIDPostProps) {
  const {data,date_now,date_week,date_month,data_all,data_country,data_theloai,path} = props
  const router = useRouter()  

  
  React.useEffect(()=>{
    if(!router.isReady) return;
  },[router.isReady])
  return (
    <div className={styles.main}>
      <Head>
        <title>Phim chills - {data?.name_vn} - {data?.name_en} </title>
        <meta property="og:image" content={data?.link_background}/>
        <meta property="og:description" content={data?.content} />
		<meta property="description" content={data?.content} />
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta property="og:title" content={"Phim chills - Thông tin phim - " +data?.name_vn +","+data?.name_en } />
        <meta name="keywords" content={data?.name_vn +", "+ data?.name_en+", phimchills , phimchills.com"} />
        <meta property="og:type" content="video.movie" />
        <meta property="og:url" content={path} />
        <meta charSet="utf-8"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>
        <main >
        <UiHead data_theloai={data_theloai} data_country={data_country} />
        <div className='container'>
          <div className='row'>
            <div className='col-xl-9'>
              <UiInfoFilm ret_link={data}/>
              <UiHomeFilm title={"Có thể bạn muốn xem"} url="/all" data={data_all}/>
            </div>
            <div className='col-lg row'>
                <UiTrending title={"TRENDING"} date_now={date_now} date_week={date_week} date_month={date_month}/>
            </div>
          </div>
          <UiFooter />
        </div>
      </main>
    </div>
  );
}
export const getServerSideProps= async (context:any)=> { 
  let param_ep:IntroLinkProp={link: context.params.InfoID }
  await APIGetUpdateView(param_ep)
  let res = await APIGetIntroLink(param_ep)
  const data = await res.data
  let ret_trend = await APIGetTrend()
  const date_now = await ret_trend?.result_now
  let ret = await APIGetCarousel()
  const data_all = await ret.data
  const date_week = await ret_trend?.result_week
  const date_month = await ret_trend?.result_month
  let ret_theloai = await APIGetTheLoai()
    let ret_country = await APIGetCountry()
    const data_theloai= await ret_theloai.data
    const data_country = await ret_country.data
    const path = "https://phimchills.com/info/"+context.params.InfoID
  return { props: { data ,date_now,date_week,date_month,data_all,data_country,data_theloai,path} }
}
