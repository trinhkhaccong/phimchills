import Head from 'next/head';
import { useRouter } from 'next/router';
import React,{useState,useEffect} from 'react';
import { json } from 'stream/consumers';
import UiCome from '../../component/Ui/UiCome';
import UiHead from '../../component/Ui/UiHead';
import styles from "../../styles/Home.module.scss"
import UiTrending from '../../component/Ui/UiTrending';
import UiFooter from '../../component/Ui/UiFooter';
import UiListFilm from '../../component/Ui/UiListFilm';
import { APIGetIntroGenre,APIGetIntroTheLoai,GetIntroTheLoaiProp,APIGetTrend,APIGetTheLoai,APIGetCountry } from '../../component/Func/APIGet';
import { Pagination } from 'antd';

export interface GenrePostProps {
  data_phim:any;
  total:any;
  title:any;
  pageNumber:any;
  date_now:any;
  date_week:any;
  date_month:any;
  data_theloai:any;
  data_country:any;
  path:any;
}

export default function GenrePost(props: GenrePostProps) {
  const {data_phim,total,title,pageNumber,date_now,date_week,date_month,data_country,data_theloai,path} = props
  const router = useRouter()

  const onChange = async(page:any,pageSize:any) => {  
    router.push({
      pathname: '/genre/'+router.query.genreID,
      query: { 
        page:page
       },
    }) 
  };

  React.useEffect(()=>{
    if(!router.isReady) return;
  },[router.isReady])
  return (
    <div className={styles.main}>
      <Head>
        <title>Phim chills - Thể loại - {title} - Phim mới cập nhật trong ngày</title>
        <meta property="og:image" content={data_phim[0]?.link_background}/>
        <meta property="og:description" content={"Phim chills - "+title+" - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày."} />
		 <meta property="description" content={"Phim chills - "+title+" - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày."} />
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta property="og:title" content={"Phim chills - Thể loại - "+title+" - phim mới cập nhật trong ngày"} />
        <meta name="keywords" content={title+ ", phimchills , phimchills.com"} />
        <meta property="og:type" content="video.movie" />
        <meta property="og:url" content={path} />
        <meta charSet="utf-8"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
        <UiHead data_theloai={data_theloai} data_country={data_country}/>
        <div className='container'>
          <div className='row'>
            <div className='col-xl-9'>
              <UiListFilm data={data_phim} title={"Thể loại: "+title} />
              <Pagination defaultCurrent={pageNumber} total={total} pageSize={24} className='m-3' onChange={(page:any,pageSize:any)=>onChange(page,pageSize)}/>
            </div>
            <div className='col-lg row'>
              
                <UiTrending title={"TRENDING"} date_now={date_now} date_week={date_week} date_month={date_month}/>
            </div>
          </div>
          <UiFooter />
        </div>
        
      </main>
      
    </div>
  );
}

export const getServerSideProps= async (context:any)=> {
  let pageNumber:any = context.query?.page ||1
  let ret_phim = await APIGetIntroGenre({link:context.query.genreID,offset:(pageNumber-1)*24,limit:24})
  const total = await ret_phim.total_count
  const title = await ret_phim.title
  const data_phim = await ret_phim.data
  let ret_trend = await APIGetTrend()
  const date_now = await ret_trend?.result_now
  const date_week = await ret_trend?.result_week
  const date_month = await ret_trend?.result_month
  let ret_theloai = await APIGetTheLoai()
    let ret_country = await APIGetCountry()
    const data_theloai= await ret_theloai.data
    const data_country = await ret_country.data
    const path = "https://phimchills.com/genre/"+context.params.genreID
  return { props: { data_phim:data_phim,total:total,title:title,pageNumber:pageNumber,date_now,date_week,date_month,data_country,data_theloai,path } }
}
