// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import { excuteQuery,excuteQueryOb } from "../../lib/db"
import { SetRedis, GetRedis } from '../../lib/redis';

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader('Cache-Control', `s-maxage=60, stale-while-revalidate`)

    let ret_redis = await GetRedis(req.url)
    if (ret_redis) {

        return res.status(200).json(ret_redis)

    }
    else {
    let check_link= req.query.link === undefined ? 'anime-hoat-hinh' : req.query.link
    let link = "%"+check_link+"%"

    const LIMIT= req.query.limit?req.query.limit:8
    const OFFSET= req.query.offset?req.query.offset:0

    const result_title = await excuteQueryOb({
        query: 'SELECT name FROM tb_theloai WHERE ?',
        values: [{link:check_link}],
    });

    const result_total = await excuteQueryOb({
        query: 'SELECT COUNT(*) as total_count FROM tb_filmintro WHERE list_link_theloai LIKE ?',
        values: [link],
    });
    const result = await excuteQuery({
        query: 'SELECT ep_today,link, link_background,name_vn FROM `tb_filmintro` WHERE list_link_theloai LIKE ? ORDER BY timestamp DESC LIMIT '+LIMIT +' OFFSET '+OFFSET,
        values: [link],
    });
    await SetRedis(req.url, JSON.stringify({ data: result ,total_count:result_total.total_count,title:result_title.name}))
    return res.status(200).json({ data: result ,total_count:result_total.total_count,title:result_title.name})
}
}
