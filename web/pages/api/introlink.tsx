// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import { excuteQueryOb } from "../../lib/db"
import { SetRedis, GetRedis } from '../../lib/redis';

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader('Cache-Control', `s-maxage=60, stale-while-revalidate`)
    let ret_redis = await GetRedis(req.url)
    if (ret_redis) {

        return res.status(200).json(ret_redis)

    }
    else {
    const result = await excuteQueryOb({
        query: 'SELECT ep_today,list_theloai,year,director,cast,content,tb_filmintro.link as link,link_background,tb_filmintro.name_vn as name_vn,tb_filmintro.name_en as name_en,time,tb_quocgia.name as country FROM tb_filmintro ,tb_quocgia where tb_filmintro.? AND tb_filmintro.id_quocgia=tb_quocgia.id',
        values: [{ link: req.query.link }]
    });
    await SetRedis(req.url, JSON.stringify({ data: result }))

    return res.status(200).json({ data: result })
}
}
