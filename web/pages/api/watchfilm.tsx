// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import { excuteQueryOb } from "../../lib/db"
import { SetRedis, GetRedis } from '../../lib/redis';

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader('Cache-Control', `s-maxage=60, stale-while-revalidate`)    
    let ob = {
        root_link: req.query.root_link,
    }
    let obep = {
        episode: req.query.episode
    }
    if(req.query.episode === undefined)
    {
        const result = await excuteQueryOb({
            query: 'SELECT  id_typephim,tb_filmintro.name_vn as name_vn,tb_filmintro.name_en as name_en,link_video_tm,link_video_sub,episode,content,link_background FROM tb_film, tb_filmintro where tb_film.root_link = tb_filmintro.link AND tb_film.?',
            values: [ob],
        });
        return res.status(200).json({ data: result })
    }
    else
    {
        const result = await excuteQueryOb({
            query: 'SELECT  id_typephim,tb_filmintro.name_vn as name_vn,tb_filmintro.name_en as name_en,link_video_tm,link_video_sub,episode,content,link_background FROM tb_film, tb_filmintro where tb_film.root_link = tb_filmintro.link AND tb_film.? AND ?',
            values: [ob, obep],
        });
        return res.status(200).json({ data: result })
    }
   
}
