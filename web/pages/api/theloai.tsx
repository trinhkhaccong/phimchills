// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import { excuteQuery } from "../../lib/db"
import { SetRedis, GetRedis } from '../../lib/redis';

export default async function getServerSideProps(
  req: NextApiRequest,
  res: NextApiResponse
) {
  res.setHeader('Cache-Control', `s-maxage=60, stale-while-revalidate`)
  let ret_redis = await GetRedis("theloai")
  if (ret_redis) {

    return res.status(200).json(ret_redis)

  }
  else {
    const result = await excuteQuery({
      query: 'SELECT link,name FROM tb_theloai',
      values: [],
    });
    let data = { data: result }
    await SetRedis("theloai", JSON.stringify(data))
    return res.status(200).json({ data: result })
  }
}
