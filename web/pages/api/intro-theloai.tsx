// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import { excuteQuery } from "../../lib/db"
import { SetRedis, GetRedis } from '../../lib/redis';

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader('Cache-Control', `s-maxage=60, stale-while-revalidate`)

    let list_link_theloai="%"+req.query.list_link_theloai+"%"
    let ret_redis = await GetRedis(req.url)
    if (ret_redis) {

        return res.status(200).json(ret_redis)

    }
    else {
   
    const result = await excuteQuery({
        query: 'SELECT ep_today,year, link, link_background,name_vn FROM `tb_filmintro` WHERE list_link_theloai LIKE ?  ORDER BY timestamp LIMIT 8',
        values: [list_link_theloai],
    });
    await SetRedis(req.url, JSON.stringify({ data: result }))

    return res.status(200).json({ data: result })
}
}
