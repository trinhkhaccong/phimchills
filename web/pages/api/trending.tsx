// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import { excuteQuery } from "../../lib/db"
import { SetRedis, GetRedis } from '../../lib/redis';

export default async function getServerSideProps(
  req: NextApiRequest,
  res: NextApiResponse
) {
  res.setHeader('Cache-Control', `s-maxage=60, stale-while-revalidate`)
  let ret_redis = await GetRedis("trending")
  if (ret_redis) {

    return res.status(200).json(ret_redis)

  }
  else {
  const result_now = await excuteQuery({
    query: 'SELECT link,link_background,name_vn,name_en,ep_today,SUM(views) as view FROM tb_filmintro,tb_view WHERE tb_filmintro.link = tb_view.root_link AND date BETWEEN curdate()-1 AND curdate() GROUP BY tb_view.root_link ORDER BY view DESC LIMIT 10',
    values: [],
  });
  
  const result_week = await excuteQuery({
    query: 'SELECT link,link_background,name_vn,name_en,ep_today,SUM(views) as view FROM tb_filmintro,tb_view WHERE tb_filmintro.link = tb_view.root_link AND date BETWEEN curdate()-7 AND curdate() GROUP BY tb_view.root_link ORDER BY view DESC LIMIT 10',
    values: [],
  });

  const result_month = await excuteQuery({
    query: 'SELECT link,link_background,name_vn,name_en,ep_today,SUM(views) as view FROM tb_filmintro,tb_view WHERE tb_filmintro.link = tb_view.root_link AND date BETWEEN curdate()-30 AND curdate() GROUP BY tb_view.root_link ORDER BY view DESC LIMIT 10',
    values: [],
  });

  await SetRedis("trending", JSON.stringify({ result_now,result_week,result_month }))
  return res.status(200).json({ result_now,result_week,result_month })
}
}
