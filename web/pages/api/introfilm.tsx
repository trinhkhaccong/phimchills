// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import { excuteQuery,excuteQueryOb } from "../../lib/db"
import { SetRedis, GetRedis } from '../../lib/redis';

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader('Cache-Control', `s-maxage=60, stale-while-revalidate`)

    let ob = {
        id_typephim: req.query.id_typephim
    }
    const LIMIT= req.query.limit?req.query.limit:8
    const OFFSET= req.query.offset?req.query.offset:0
    let ret_redis = await GetRedis(req.url)
    if (ret_redis) {

        return res.status(200).json(ret_redis)

    }
    else {
    
    const result_total = await excuteQueryOb({
        query: 'SELECT COUNT(*) as total_count FROM tb_filmintro WHERE ?',
        values: [ob],
    });
    const result = await excuteQuery({
        query: 'SELECT ep_today,link, link_background,name_vn FROM tb_filmintro WHERE ? ORDER BY timestamp DESC LIMIT '+LIMIT +' OFFSET '+OFFSET,
        values: [ob],
    });
    await SetRedis(req.url, JSON.stringify({ data: result ,total_count:result_total.total_count}))

    return res.status(200).json({ data: result ,total_count:result_total.total_count})
}
}
