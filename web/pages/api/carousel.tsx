// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse, GetStaticProps, GetStaticPaths, GetServerSideProps } from 'next'
import { excuteQuery } from "../../lib/db"
import { SetRedis, GetRedis } from '../../lib/redis';

export default async function getServerSideProps(
    req: NextApiRequest,
    res: NextApiResponse
) {
    res.setHeader('Cache-Control', `s-maxage=60, stale-while-revalidate`)
    let ret_redis = await GetRedis("carousel")
    if (ret_redis) {

        return res.status(200).json(ret_redis)

    }
    else {
        const result = await excuteQuery({
            query: 'SELECT ep_today,link, link_background,name_vn FROM tb_filmintro ORDER BY timestamp DESC LIMIT 20',
            values: [],
        });
        await SetRedis("carousel", JSON.stringify({ data: result }))
        return res.status(200).json({ data: result })
    }
}
