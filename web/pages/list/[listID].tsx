import Head from 'next/head';
import { useRouter } from 'next/router';
import React,{useState,useEffect} from 'react';
import UiHead from '../../component/Ui/UiHead';
import styles from "../../styles/Home.module.scss"
import UiTrending from '../../component/Ui/UiTrending';
import UiFooter from '../../component/Ui/UiFooter';
import UiListFilm from '../../component/Ui/UiListFilm';
import { APIGetIntro,APIGetIntroTheLoai,GetIntroTheLoaiProp,APIGetTrend,APIGetTheLoai,APIGetCountry } from '../../component/Func/APIGet';
import { Pagination } from 'antd';

export interface ListPostProps {
  data_phim:any;
  total:any;
  pageNumber:any;
  date_now:any;
  date_week:any;
  date_month:any;
  data_theloai:any;
  data_country:any;
  path:any
}

export default function ListPost(props: ListPostProps) {
  const {data_phim,total,pageNumber,date_now,date_week,date_month,data_country,data_theloai,path} = props
  const router = useRouter()


  const onChange = async(pageNumber:any) => {
    router.push({
      pathname: '/list/'+router.query.listID,
      query: { 
        page:pageNumber
       },
    })

      // window.open("/list/"+router.query.listID+"?page="+pageNumber,"_parent")
  };

  React.useEffect(()=>{
    if(!router.isReady) return;
  },[router.isReady])
  return (
    <div className={styles.main}>
      <Head>
        <title>Phim chills - Thể loại - {router.query.listID==='phim-le'?"Phim Lẻ":"Phim Bộ"} - phim mới cập nhật trong ngày</title>
        <meta property="og:image" content={data_phim[0]?.link_background}/>
        <meta property="og:description" content="Phim chills - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày." />
		<meta property="description" content="Phim chills - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày." />
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta property="og:title" content={"Phim chills - Thể loại - phim mới cập nhật trong ngày"} />
        <meta name="keywords" content={"phimchills , phimchills.com"} />
        <meta property="og:type" content="video.movie" />
        <meta property="og:url" content={path} />
        <meta charSet="utf-8"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>
     
      <main >
        <UiHead data_theloai={data_theloai} data_country={data_country}/>
        <div className='container'>
          <div className='row'>
            <div className='col-xl-9'>
              <UiListFilm data={data_phim} title={router.query.listID==='phim-le'?"Phim Lẻ":"Phim Bộ"} />
              <Pagination defaultCurrent={pageNumber} total={total} pageSize={24} className='m-3' onChange={onChange}/>
            </div>
            <div className='col-lg row'>
                <UiTrending title={"TRENDING"} date_now={date_now} date_week={date_week} date_month={date_month}/>
            </div>
          </div>
          <UiFooter />
        </div>
      </main>
      
    </div>
  );
}

export const getServerSideProps= async (context:any)=> { 
  let pageNumber:any = context.query?.page||1
  let ret_phim = await APIGetIntro({id_typephim:context.query.listID==='phim-le'?1:2,offset:(pageNumber-1)*24,limit:24})
  const total = await ret_phim.total_count
  const data_phim = await ret_phim.data
  let ret_trend = await APIGetTrend()
  const date_now = await ret_trend?.result_now
  const date_week = await ret_trend?.result_week
  const date_month = await ret_trend?.result_month
  let ret_theloai = await APIGetTheLoai()
    let ret_country = await APIGetCountry()
    const data_theloai= await ret_theloai.data
    const data_country = await ret_country.data
    const path = "https://phimchills.com/list/"+context.params.listID

  return { props: { data_phim:data_phim,total:total,pageNumber:pageNumber,date_now,date_week,date_month,data_country,data_theloai,path } }
}
