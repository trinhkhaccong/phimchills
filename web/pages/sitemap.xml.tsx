import fs from "fs";
import { excuteQuery } from "../lib/db";
import { SetRedis, GetRedis } from '../lib/redis';

export default function Sitemap(){};

export const getServerSideProps=async({res}:any)=> {

  const production= "https://phimchills.com"
  let staticPages:any = ["https://phimchills.com/all","https://phimchills.com/country","https://phimchills.com/genre","https://phimchills.com/info","https://phimchills.com/list","https://phimchills.com/movieplay","https://phimchills.com/watch"]

  // let ret_redis = await GetRedis("sitemap.xml")
  //   if (ret_redis) {
  //     staticPages = ret_redis

  //   }
  //   else {
        const result:any = await excuteQuery({
          query: 'SELECT link FROM tb_filmintro LIMIT 10000',
          values: [],
      });
      const result_theloai:any = await excuteQuery({
        query: 'SELECT link FROM tb_theloai',
        values: [],
    });
    const result_country:any = await excuteQuery({
      query: 'SELECT link FROM tb_quocgia',
      values: [],
    });
    
      result.map((value:any)=>{
        if(value.link.search("&")<0)
        {
          if(value.link.search(",")<0)
          {
            staticPages.push(production+"/info/"+value.link)
            staticPages.push(production+"/watch/"+value.link)
          }
        }
      })
      result_theloai.map((value:any)=>{
        staticPages.push(production+"/genre/"+value.link)
      })
      result_country.map((value:any)=>{
        staticPages.push(production+"/country/"+value.link)
      })
    //     await SetRedis("sitemap.xml", JSON.stringify(staticPages))
    // }
  


  // let content = staticPages.join("\n")
  // fs.writeFile('test.txt', content, err => {
  //   if (err) {
  //     console.error(err);
  //   }
  //   else
  //   {
  //     console.log("File written successfully\n");
  //   console.log("The written has the following contents:");
  //   }
  // });
  
  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
          <url>
            <loc>${production}</loc>
            <lastmod>${new Date().toISOString()}</lastmod>
            <priority>1.0</priority>
          </url>
    ${staticPages
      .map((url:any) => {
        return `
          <url>
            <loc>${url}</loc>
            <lastmod>${new Date().toISOString()}</lastmod>
            <priority>0.8</priority>
          </url>
        `;
      })
      .join("")}
    </urlset>
  `;
  res.setHeader("Content-Type", "text/xml");
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
};