import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  // static async getInitialProps(ctx:any) {
  //   const initialProps = await Document.getInitialProps(ctx);
  //   return { ...initialProps };
  // }

  render() {
    return (
      <Html lang="vi">
        <Head>
          <meta name="propeller" content="8075d75df366cae2ea2bba2aac63295e"></meta>
          <meta name="google-site-verification" content="83U5igq10z4ttbqjYOMrev0TkBS9U8-r8UfWrYzWKhI" />
          <script src="https://choupsee.com/pfe/current/tag.min.js?z=5323798" data-cfasync="false" async ></script>
          <script src="//upgulpinon.com/1?z=5329885" async data-cfasync="false" />
          <script src="https://stootsou.net/pfe/current/tag.min.js?z=5323798" data-cfasync="false" async ></script>
          <script async src="https://www.googletagmanager.com/gtag/js?id=G-35XLKEFWG6"></script>
          <script data-cfasync="false" src="//pl17673845.profitablegatetocontent.com/971777154accb7bf0043aef787653bf6/invoke.js" async ></script>
          {/* <script
            dangerouslySetInnerHTML={{
              __html: `
              (function(d,z,s){s.src='https://'+d+'/400/'+z;try{(document.body||document.documentElement).appendChild(s)}catch(e){}})('tauvoojo.net',5323812,document.createElement('script'))
              `,
            }}
          /> */}
          <script
            dangerouslySetInnerHTML={{
              __html: `
              (function(d,z,s){s.src='https://'+d+'/401/'+z;try{(document.body||document.documentElement).appendChild(s)}catch(e){}})('glizauvo.net',5324780,document.createElement('script'))
              `,
            }}

          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
            
              gtag('config', 'G-35XLKEFWG6');
              `,
            }}

          />
        </Head>
        <body>

          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;