import Head from 'next/head';
import { useRouter } from 'next/router';
import * as React from 'react';
import { json } from 'stream/consumers';
import UiHead from '../../component/Ui/UiHead';
import styles from "../../styles/Home.module.scss"
import UiTrending from '../../component/Ui/UiTrending';
import UiFooter from '../../component/Ui/UiFooter';
import UiHomeFilm from '../../component/Ui/UiHomeFilm';
import { APIGetWatchFilm, APIGetTheLoai, APIGetEpisode, APIGetCountry, GetEpisodeProp, APIGetCarousel, APIGetIntroTheLoai, APIGetUpdateView, APIGetTrend } from "../../component/Func/APIGet"
import UiWatch from '../../component/Ui/UiWatch';
export interface WatchIDProps {
  data: any;
  episode: any;
  carousel: any;
  date_now: any;
  date_week: any;
  date_month: any;
  data_theloai: any;
  data_country: any;
  path: any;
}


interface EpisodeProp {
  episode: any;
  root_link: any;
}
export default function WatchID(props: WatchIDProps) {
  const { data, episode, carousel, date_now, date_week, date_month, data_country, data_theloai, path } = props
  const router = useRouter()

  React.useEffect(() => {
    if (!router.isReady) return;
  }, [router.isReady])
  return (
    <div className={styles.main}>
      <Head>
        <title>Phim chills - {data?.name_vn + " - " + data?.name_en}</title>
        <meta property="og:image" content={data?.link_background} />
        <meta property="og:description" content={data?.content} />
        <meta property="description" content={data?.content} />
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta property="og:title" content={"Phim chills -" + data?.name_vn + " - " + data?.name_en} />
        <meta name="keywords" content={data?.name_vn + " , " + data?.name_en + ", phimchills , phimchills.com"} />
        <meta property="og:type" content="video.movie" />
        <meta property="og:url" content={path} />
        <meta charSet="utf-8"></meta>

        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://video-react.js.org/assets/video-react.css"
        />
      </Head>
      <main >
        <UiHead data_theloai={data_theloai} data_country={data_country} />
        <div className='container'>
          <div className='row'>
            <div className='col-xl-9'>
              <UiWatch datalink={data} episode={episode} />
              <UiHomeFilm title={"Có thể bạn muốn xem "} url="/all" data={carousel} />
            </div>
            <div className='col-lg row'>
              <UiTrending title={"TRENDING"} date_now={date_now} date_week={date_week} date_month={date_month} />
            </div>
          </div>
          <UiFooter />
        </div>
      </main>
    </div>
  );
}

export const getServerSideProps = async (context: any) => {
  let list = context.params.watchID.split("-tap-")
  let param: GetEpisodeProp = { root_link: list[0] }
  let param_ep: EpisodeProp = { root_link: list[0], episode: list[1] ? list[1] : undefined }

  let res = await APIGetWatchFilm(param_ep)
  let ret_ep = await APIGetEpisode(param)
  let ret = await APIGetCarousel()
  await APIGetUpdateView({ link: list[0] })

  const episode = await ret_ep.data
  const carousel = await ret.data
  const data = await res.data

  let ret_trend = await APIGetTrend()
  const date_now = await ret_trend?.result_now
  const date_week = await ret_trend?.result_week
  const date_month = await ret_trend?.result_month
  let ret_theloai = await APIGetTheLoai()
  let ret_country = await APIGetCountry()
  const data_theloai = await ret_theloai.data
  const data_country = await ret_country.data
  const path = "https://phimchills.com/watch/" + context.params.watchID
  return { props: { data, episode, carousel, date_now, date_week, date_month, data_theloai, data_country, path } }
}
