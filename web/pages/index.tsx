import type { NextPage } from 'next'
import Head from 'next/head'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import UiHead from '../component/Ui/UiHead'
import styles from '../styles/Home.module.scss'
import UiCarousel from '../component/Ui/UiCarousel'
import UiHomeFilm from '../component/Ui/UiHomeFilm'
import UiFooter from '../component/Ui/UiFooter'
import UiTrending from '../component/Ui/UiTrending'
import UiReload from '../component/Ui/UiReload'
import Script from 'next/script'
// import { Html, Head, Main, NextScript } from 'next/document'
import {APIGetIntro,APIGetSlider,APIGetIntroTheLoai,GetIntroProp,GetIntroTheLoaiProp,APIGetTrend,APIGetTheLoai,APIGetCountry} from "../component/Func/APIGet"
interface HomeProp
{
  data_phimle:any;
  data_phimbo:any;
  cinema:any;
  anime:any;
  data_carousel:any;
  date_now:any;
  date_week:any;
  date_month:any;
  data_theloai:any;
  data_country:any;
  data_chieu_rap:any;
}
const Home = (props:HomeProp) => {
  const {data_phimle,data_phimbo,cinema,anime,data_carousel,date_now,date_week,date_month,data_theloai,data_country,data_chieu_rap }=props
  const router = useRouter()
  const [playTime, setPlayTime] = useState(0);

  React.useEffect(()=>{
    if(!router.isReady) return;
   
  },[router.isReady])
return (
    <div className={styles.main}>
      <Head>
        <title>Phim chills - Kho phim tổng hợp nhanh nhất, hay nhất, mới nhất</title>
        <meta property="og:image" content={data_carousel[0]?.link_background}/>
        <meta property="og:description" content="Phim chills - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày." />
         <meta property="description" content="Phim chills - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày." />
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta property="og:title" content="Phim chills - Kho phim tổng hợp nhanh nhất, hay nhất, mới nhất" />
        <meta name="keywords" content="phimchills , phimchills.com" />
        <meta property="og:type" content="video.movie" />
        <meta property="og:url" content="https://phimchills.com" />
        <meta charSet="utf-8"></meta>

        <meta name="propeller" content="8075d75df366cae2ea2bba2aac63295e"></meta>
        <meta name="google-site-verification" content="83U5igq10z4ttbqjYOMrev0TkBS9U8-r8UfWrYzWKhI" />
		    <meta name="clckd" content="ec096bb9988efd11c66885b429ab3023" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
          <UiHead data_theloai={data_theloai} data_country={data_country}/>
          <div className='container'>
            <UiCarousel  data={data_carousel}/>
            <div className='row'>
              <div className='col-xl-9'>
                <UiHomeFilm title={"PHIM LẺ MỚI CẬP NHẬT"} url="/list/phim-le" data={data_phimle}/>
                <UiHomeFilm title={"PHIM BỘ MỚI CẬP NHẬT"} url="/list/phim-bo" data={data_phimbo}/>
                <UiHomeFilm title={"PHIM ANIME - HOẠT HÌNH"} url="/genre/phim-anime" data={anime}/>
				 <UiHomeFilm title={"PHIM CHIẾU RẠP"} url="/genre/phim-chieu-rap" data={data_chieu_rap}/>
              </div>
              <div className='col-lg row'>
                  <UiTrending title={"TRENDING"} date_now={date_now} date_week={date_week} date_month={date_month}/>
              </div>
            </div>
            <UiFooter/>
          </div>
        </main> 
    </div>
  )
}

export const getServerSideProps= async (context:any)=> { 
    let ret_carousel = await APIGetSlider()
    let param_phimle:GetIntroProp={id_typephim:1,limit:8,offset:0}
    let param_phimbo:GetIntroProp={id_typephim:2,limit:8,offset:0}
    let param_anime:GetIntroTheLoaiProp={list_link_theloai:"phim-anime",limit:8,offset:0}
	let param_chieu_rap:GetIntroTheLoaiProp={list_link_theloai:"phim-chieu-rap",limit:8,offset:0}
    
    
    let ret_phimle = await APIGetIntro(param_phimle)
    let ret_phimbo = await APIGetIntro(param_phimbo)
    let ret_anime = await APIGetIntroTheLoai(param_anime)
	let ret_chieu_rap = await APIGetIntroTheLoai(param_chieu_rap)
    const data_phimle = await ret_phimle.data
    const data_phimbo = await ret_phimbo.data
	const data_chieu_rap = await ret_chieu_rap.data
    const anime = await ret_anime.data
    const data_carousel= await ret_carousel.data
    let ret_trend = await APIGetTrend()
    const date_now = await ret_trend?.result_now
    const date_week = await ret_trend?.result_week
    const date_month = await ret_trend?.result_month
    
    let ret_theloai = await APIGetTheLoai()
    let ret_country = await APIGetCountry()
    const data_theloai= await ret_theloai.data
    const data_country = await ret_country.data
  return { props: { data_phimle,data_phimbo,anime,data_chieu_rap,data_carousel,date_now,date_week,date_month,data_theloai,data_country } }
}

export default Home
