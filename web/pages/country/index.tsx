import * as React from 'react';
import Head from 'next/head';
import UiHead from '../../component/Ui/UiHead';
export interface CountryPageProps {
}

export default function CountryPage (props:CountryPageProps) {
  return (
    <div>
       <Head>
       <title>Phim chills - xem phim hay -  - phim mới cập nhật trong ngày</title>
        <meta property="og:image" content={"http://phimchills.com/phimchills.png"}/>
        <meta property="og:description" content="Phim chills - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày." />
        <link rel="icon" href="/favicon.ico" />
        </Head>
        <main>
          Danh sách phim
        </main>
    </div>
  );
}
