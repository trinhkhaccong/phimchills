import Head from 'next/head';
import { useRouter } from 'next/router';
import React,{useState,useEffect} from 'react';
import UiHomeFilm from '../../component/Ui/UiHomeFilm';
import UiHead from '../../component/Ui/UiHead';
import styles from "../../styles/Home.module.scss"
import UiTrending from '../../component/Ui/UiTrending';
import UiFooter from '../../component/Ui/UiFooter';
import UiListFilm from '../../component/Ui/UiListFilm';
import {APIGetIntroSearch,GetIntroSearchProp,APIGetTheLoai,APIGetCountry,APIGetTrend,APIGetCarousel } from '../../component/Func/APIGet';
import { Pagination } from 'antd';

export interface SearchPostIDProps {
  ret_phim:any;
  date_now:any;
  date_week:any;
  date_month:any;
  carousel:any;
  pageNumber:any;
  data_theloai:any;
  data_country:any;
}

export default function SearchPostID(props: SearchPostIDProps) {
  const {date_now,date_week,date_month,carousel,ret_phim,pageNumber,data_country,data_theloai}= props
  const router = useRouter()
  const onChange = async(pageNumber:any) => {
    router.push({
      pathname: '/search',
      query: { 
        key:router.query.key,
        page:pageNumber
       },
    })
  };

  React.useEffect(()=>{
    if(!router.isReady) return;
  },[router.isReady])
  return (
    <div className={styles.main}>
      <Head>
        <title>Phim chills - Tìm kiếm phim - {ret_phim.key} - phim mới cập nhật trong ngày</title>
        <meta property="og:image" content={"http://phimchills.com/phimchills.png"}/>
        <meta property="og:description" content="Phim chills - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày." />
		<meta property="description" content="Phim chills - Phim Chills chất lượng cao miễn phí. Xem phim hd VietSub. Phim thuyết minh chất lượng HD. Phim bộ mới nhất, phim hành động , anime,kinh dị ... hay nhất. Cập nhật tất cả các phim mới nhất, nhanh nhất trong ngày." />
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
        <meta property="og:title" content={"Phim chills - Phim đang chiếu - phim mới cập nhật trong ngày"} />
        <meta name="keywords" content={"phimchills , phimchills.com"} />
        <meta property="og:type" content="video.movie" />
        <meta charSet="utf-8"></meta>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main >
        <UiHead data_theloai={data_theloai} data_country={data_country}/>
        <div className='container'>
          <div className='row'>
            <div className='col-xl-9'>
              <UiListFilm data={ret_phim.data} title={"Từ khóa tìm kiếm : "+ret_phim.key} />
              <Pagination defaultCurrent={pageNumber} total={ret_phim.count_total} pageSize={24} className='m-3' onChange={onChange}/>
              <UiHomeFilm title={"Có thể bạn muốn xem "} url="/all" data={carousel}/>

            </div>
            <div className='col-lg row'>
                <UiTrending title={"TRENDING"} date_now={date_now} date_week={date_week} date_month={date_month}/>
            </div>
          </div>
          <UiFooter />
        </div>
        
      </main>
      
    </div>
  );
}
export const getServerSideProps= async (context:any)=> { 
    let pageNumber:any = context.query?.page||1
    let key:any = encodeURIComponent(context.query?.key)
    let param:GetIntroSearchProp={s_search:key,offset:(pageNumber-1)*24,limit:24}
    let ret_phim = await APIGetIntroSearch(param)
    let ret_trend = await APIGetTrend()
    let ret = await APIGetCarousel()
    const carousel = await ret.data
    const date_now = await ret_trend?.result_now
    const date_week = await ret_trend?.result_week
    const date_month = await ret_trend?.result_month
    let ret_theloai = await APIGetTheLoai()
    let ret_country = await APIGetCountry()
    const data_theloai= await ret_theloai.data
    const data_country = await ret_country.data
  return { props: { date_now,date_week,date_month,carousel,ret_phim,pageNumber,data_country,data_theloai } }
}