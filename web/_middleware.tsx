import { NextResponse } from 'next/server';
import type { NextRequest } from 'next/server';

export function middleware(req: NextRequest) {
  const { pathname } = req.nextUrl;
  
  if (pathname.startsWith('/')) {
    return NextResponse.redirect(new URL('/', req.nextUrl));
  }
}

// Supports both a single string value or an array of matchers
export const config = {
  matcher: ['/info/:path*', '/watch/:path*','/search/:path*', '/movieplay/:path*','/genre/:path*', '/country/:path*'],
}