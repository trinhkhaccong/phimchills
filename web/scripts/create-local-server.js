const { createServer: createHttpsServer } = require('https');
const next = require('next');
const fs = require('fs');
const chalk = require('chalk');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev:false });
const handle = app.getRequestHandler();
const PORT = 443;


app
  .prepare()
  .then(() => {
    const server = createHttpsServer(
      {
        key: fs.readFileSync('./certs/phimchills.com.key'),
        cert: fs.readFileSync('./certs/phimchills.com.cert'),
      },
      (req, res) => handle(req, res)
    );

    return server.listen(PORT, (err) => {
      if (err) throw err;

      console.log('> Ready on https://phimchills.com')
    });
  })
  .catch((err) => {
    console.error(err);
  });