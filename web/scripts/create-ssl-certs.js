const devcert = require('devcert');
const fs = require('fs');

if (!fs.existsSync('./certs')) {
  fs.mkdirSync('./certs');
}

const domains = ['phimchills.com'];

devcert
  .certificateFor(domains, { getCaPath: true })
  .then(({ key, cert }) => {
    fs.writeFileSync('./certs/phimchills.com.key', key);
    fs.writeFileSync('./certs/phimchills.com.cert', cert);
  })
  .catch(console.error);