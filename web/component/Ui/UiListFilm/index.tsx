import * as React from 'react';
import { useState, useEffect } from 'react';
import { Card } from 'antd';
import styles from "../../../styles/Home.module.scss"
import Link from 'next/link';
const gridStyle1: React.CSSProperties = {
    width: "25%",
    textAlign: 'center',
    color: "#FFFFFF",
    backgroundColor:"#1a1a1a"
};

const gridStyle2: React.CSSProperties = {
    width: "33%",
    textAlign: 'center',
    color: "#FFFFFF",
    backgroundColor:"#1a1a1a"

};

const gridStyle3: React.CSSProperties = {
    width: "50%",
    textAlign: 'center',
    color: "#FFFFFF",
    backgroundColor:"#1a1a1a"

};
interface ObjectFilm
{
    link: string;
    link_background: string;
    name_vn: string;
    ep_today:any;
}
export interface UiListFilmProps {
    title: string;
    data:ObjectFilm[]
}

const UiListFilm = (props: UiListFilmProps) => {
    const [position, setPosition] = useState(0);

    useEffect(() => {
        setPosition(window.innerWidth)
    }, [position]);

    return (
        <div >
            <div className='d-flex justify-content-between mt-3'>
            <div className='title-film' > {props.title}</div>
            </div>
			<div id="SC_TBlock_875508"></div>
            <Card>
            {
                    props.data?.map(value => (
                        <Card.Grid style={position > 1100 ? gridStyle1 : (position > 765 ? gridStyle2 : gridStyle3)}  key={"info-list-girl-"+value.link}>
                            <Link href={"/info/"+value.link} key={"info-list-"+value.link}>
                            <div className={styles.home}>
                                <img alt={value.name_vn} className={styles.imageHome} src={value.link_background}/>
                                <div className={styles.bottomHome}>{value.name_vn}</div>
                                <div className={styles.topLeftHome}>{value.ep_today}</div>
                                <div className={styles.play}></div>
                            </div>
                            </Link>
                        </Card.Grid>
                    ))
                }
            </Card>
            <div id="container-971777154accb7bf0043aef787653bf6"></div>

        </div>
    );
}

export default UiListFilm