import * as React from 'react';
import { Card } from 'antd';
import type { RadioChangeEvent } from 'antd';
import { Radio, Tabs } from 'antd';
import type { SizeType } from 'antd/es/config-provider/SizeContext';
import { useState } from 'react';
import Link from 'next/link';
const { TabPane } = Tabs;



const gridStyle: React.CSSProperties = {
    width: "100%",
    textAlign: 'center',
    color: "#FFFFFF",
    backgroundColor: "#1a1a1a",
    cursor: "pointer"
};

export interface UiTrendingProps {

    title: string;
    date_now: any;
    date_week:any;
    date_month:any;
}

const UiTrending = (props: UiTrendingProps) => {
    const {title,date_now,date_week,date_month} = props
    const [size, setSize] = useState<SizeType>('small');
    
    return (
        <div >
			<script type="text/javascript" src="https://historicalcarawayammonia.com/f55de7ac0991277bb24221eecf1cdecc/invoke.js" async></script>
            <div className='title-film' > {title}</div>
            <div>
                <Tabs defaultActiveKey="1" type="card" size={size}>
                    <TabPane tab="Ngày" key="1">
                        <Card>
                            {
                                date_now?.map((value:any) => (
                                    <Card.Grid style={gridStyle} key={value.link+"-trend-gird"}>
                                        <Link href={"/info/"+value.link} key={value.link+"-trend"}>
                                        <div className='div-trend d-flex' style={{textAlign:"left"}}>
                                            <img className='image-home p-1'  alt={value.name} width="120px" height='170px' style={{objectFit:"cover"}} src={value.link_background} />
                                            <div className='col p-1'>
                                                <div style={{paddingLeft:5}}>{value.name_vn}</div>
                                                <div className='centered-left-name'>{value.name_en}</div>
                                                <div className='centered-left-tap'>Hiện tại: {value.ep_today}</div>
                                                <div className='centered-left-view'>{value.view} lượt xem</div>
                                            </div>
                                        </div>
                                        </Link>
                                    </Card.Grid>
                                ))
                            }
                        </Card>
                    </TabPane>
                    <TabPane tab="Tuần" key="2">
                        <Card>
                            {
                                date_week?.map((value:any) => (
                                    <Card.Grid style={gridStyle} key={value.link+"-trend-gird"}>
                                         <Link href={"/info/"+value.link} key={value.link+"-trend"}>
                                        <div className='div-trend d-flex' style={{textAlign:"left"}}>
                                            <img className='image-home p-1'  alt={value.name} width="120px" height='170px' style={{objectFit:"cover"}} src={value.link_background} />
                                            <div className='col p-1'>
                                                <div style={{paddingLeft:5}}>{value.name_vn}</div>
                                                <div className='centered-left-name'>{value.name_en}</div>
                                                <div className='centered-left-tap'>Hiện tại: {value.ep_today}</div>
                                                <div className='centered-left-view'>{value.view} lượt xem</div>
                                            </div>
                                        </div>
                                        </Link>
                                    </Card.Grid>
                                ))
                            }
                        </Card>
                    </TabPane>
                    <TabPane tab="Tháng" key="3">
                        <Card>
                            {
                                date_month?.map((value:any) => (
                                    <Card.Grid style={gridStyle} key={value.link+"-trend-gird"}>
                                         <Link href={"/info/"+value.link} key={value.link+"-trend"}>
                                        <div className='div-trend d-flex' style={{textAlign:"left"}}>
                                            <img className='image-home p-1'  alt={value.name} width="120px" height='170px' style={{objectFit:"cover"}} src={value.link_background} />
                                            <div className='col p-1'>
                                                <div style={{paddingLeft:5}}>{value.name_vn}</div>
                                                <div className='centered-left-name'>{value.name_en}</div>
                                                <div className='centered-left-tap'>Hiện tại: {value.ep_today}</div>
                                                <div className='centered-left-view'>{value.view} lượt xem</div>
                                            </div>
                                        </div>
                                        </Link>
                                    </Card.Grid>
                                ))
                            }
                        </Card>
                    </TabPane>
                </Tabs>
            </div>
        </div>
    );
}
export default UiTrending