import * as React from 'react';
import styles from '../../../styles/Home.module.scss'
import { SearchOutlined} from '@ant-design/icons';
import { Input } from 'antd';
import { useRouter } from 'next/router';
export interface UiSearchProps {
}
const UiSearch = (props: UiSearchProps) => {
  const router = useRouter()
  const [key,setkey] = React.useState("")
  const onEnter=()=>{
    router.push("/search?key="+key.replaceAll(" ","+"))
  }
  
  React.useEffect(() => {
  }, [])
  return (
    <div className={styles.centered}>
      <Input placeholder="Tên tác giả, Tên phim" prefix={<SearchOutlined />} onChange={(e)=>setkey(e.target.value)} onPressEnter={onEnter} />
    </div>

  );
}
export default UiSearch