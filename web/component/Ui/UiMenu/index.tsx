import * as React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

export interface UiMenuProps {
  data_theloai:any;
  data_country:any;
}

const UiMenu = (props: UiMenuProps) => {
  const {data_theloai,data_country} = props
  const router = useRouter()

  React.useEffect(()=>{
    if(!router.isReady) return;   
  },[router.isReady])
  return (
    <div style={{ color: "#FFFFFF" }} className="p-3">
      <div className="d-flex justify-content-between">
        <Link href="/"><div className=' menu'>Trang Chủ</div></Link>
        <div className='dropdown'>
          <span className=' menu'>Thể Loại</span>
          <div className="dropdown-content">
          {
             data_theloai.map((value:any,index:any) => (index%3===0) &&(
                <div className='row' style={{textAlign:"left"}}>
                  <Link href ={"/genre/"+value.link}><a  className='col dropdown-text'>
                    {value.name}
                  </a></Link>
                  <Link href ={"/genre/"+data_theloai[index+1]?.link}><a  className='col dropdown-text'>
                    {data_theloai[index+1]?.name}
                  </a></Link>
                  <Link href ={"/genre/"+data_theloai[index+2]?.link}><a className='col dropdown-text'>
                    {data_theloai[index+2]?.name}
                  </a>
                  </Link>
                </div>
              ))
            }
          </div>
        </div>
        <div className='dropdown'>
          <span className=' menu'>Quốc gia</span>
          <div className="dropdown-content">
          {
             data_country.map((value:any,index:any) => (index%3===0) &&(
                <div className='row' style={{textAlign:"left"}}>
                  <Link href ={"/country/"+value.link}><a  className='col dropdown-text'>
                    {value.name}
                  </a></Link>
                  <Link href ={"/country/"+data_country[index+1]?.link}><a  className='col dropdown-text'>
                    {data_country[index+1]?.name}
                  </a></Link>
                  <Link href ={"/country/"+data_country[index+2]?.link}><a  className='col dropdown-text'>
                    {data_country[index+2]?.name}
                  </a></Link>
                </div>
              ))
            }
          </div>
        </div>
		<Link href="/all"><div className=' menu'>Phim Mới</div></Link>
        <Link href="/genre/phim-anime"><div className=' menu'>Anime - Hoạt Hình</div></Link>
        <Link href="/list/phim-le"><div className=' menu'>Phim Lẻ</div></Link>
        <Link href="/list/phim-bo"><div className=' menu'>Phim Bộ</div></Link>
		<Link href="/genre/phim-chieu-rap"><div className=' menu'>Phim Chiếu Rạp</div></Link>
      </div>

    </div>
  );
}

export default UiMenu