import * as React from 'react';
import UiSearch from '../UiSearch';
import UiMenu from '../UiMenu';
export interface UiFooterProps {
}

const UiFooter =(props:UiFooterProps)=> {
  return (
    <div >
        <div className='row justify-content-between' style={{backgroundColor:"#1a1a1a",padding:15,textAlign:"center"}}>
          <div className='col-md-3' style={{ color: "#FFFFFF", fontWeight: 'bold' }}>
            <img alt="Phim chills" width='100%' src="https://phimchills.com/phimchills.png" />
            <div>Liên hệ: phimchills@gmail.com</div>
            <div> Page facebook: <a href='https://facebook.com/phimchills'>Phim Chills</a></div>
            </div>
          <div className='col-md-9' style={{textAlign:"left",color: "#FFFFFF"}}>
            <div>Phimchills - phim không chứa quảng cáo - Cập nhật phim nhanh nhất 24/24 - Xem phim online miễn phí chất lượng cao với phụ đề tiếng việt - thuyết minh - lồng tiếng. Mọt phim có nhiều thể loại phim phong phú, đặc sắc, nhiều bộ phim hay nhất - mới nhất.</div>
            <br/>
              <div>Website phimchills.com với giao diện trực quan, thuận tiện, tốc độ tải nhanh, thường xuyên cập nhật các bộ phim mới hứa hẹn sẽ đem lại những trải nghiệm tốt cho người dùng.</div>
              </div>
        </div>
    </div>
    
  );
}
export default UiFooter