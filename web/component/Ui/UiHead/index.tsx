import * as React from 'react';
import UiSearch from '../UiSearch';
import UiMenu from '../UiMenu';
import { MenuUnfoldOutlined } from "@ant-design/icons"
import UiMenuMobile from '../UiMenu/MenuMobile';
import { useRouter } from 'next/router';

export interface UiHeadProps {
  data_theloai:any;
  data_country:any;
}



const UiHead = (props: UiHeadProps) => {
  const {data_theloai,data_country} = props
  const [position, setPosition] = React.useState(true);
  const [openMenu, setOpenMenu] = React.useState(false);
  const router = useRouter()

  const GetWidth = async () => {
    await setPosition(window.innerWidth > 999 ? false : true)
  }
  React.useEffect(() => {
    if(!router.isReady) return;   
    GetWidth()
  }, [position, props, openMenu,router.isReady]);


  return (
    <div className='border-menu' style={{ backgroundColor: "rgba(0,0,0,0.3)" }}>
      <div className='container'>

        <UiMenuMobile openMenu={openMenu} setCloseMenu={() => setOpenMenu(false)} />
        {
          !position &&

          <div>
            <div className='d-flex p-5' style={{ backgroundColor: "#1a1a1a", alignItems: 'end' }}>
              <div className='col-md-3' style={{ color: "#FFFFFF", fontWeight: 'bold' }}><img onClick ={()=>window.open("/","_parent")} style={{cursor:'pointer'}} alt="Phim chills" width='90%' src="https://phimchills.com/phimchills.png"/></div>
              <div className='col-md-8' style={{ paddingBottom: 5 }}><UiSearch /></div>
            </div>
            <UiMenu data_theloai={data_theloai} data_country={data_country}/>
          </div>
        }
        {
          position && <div><div className='d-flex justify-content-between' style={{ backgroundColor: "#1a1a1a", padding: 15,alignItems: 'end' }}>
            <div><MenuUnfoldOutlined style={{ color: "#FFFFFF", fontSize: 35, cursor: "pointer", fontWeight: 'bold' }} onClick={async () => await setOpenMenu(true)} /></div>
            <div className='col-md-4' style={{ color: "#FFFFFF", fontWeight: 'bold' }}><img style={{cursor:'pointer'}} onClick ={()=>window.open("/","_parent")} alt="Phim chills" width='100%' src="https://phimchills.com/phimchills.png" /></div>
            <div className='col-md-1'></div>
          </div>
            <div className='col-12' style={{ padding: 10 }}><UiSearch /></div>
          </div>
        }
      </div>

    </div>

  );
}
export default UiHead