import { useRouter } from 'next/router';
import * as React from 'react';
import {useEffect,useState} from "react"
import Slider from "react-slick";
import Link from 'next/link';
import styles from "../../../styles/Home.module.scss"
interface ObjectFilm
{
    link: string;
    link_background: string;
    name_vn: string;
    ep_today:any;
}
export interface UiCarouselProps {
  data:ObjectFilm[]
}

const UiCarousel =(props:UiCarouselProps)=> {
  const [position, setPosition] = useState(0);
  const router = useRouter()
    const renderSlides = () =>
    props.data?.map(value => (
      <Link href={"/info/"+value.link} key={"carousel-info-"+value.link}>
        <label className={styles.slider} >
          <img alt={value.name_vn} className={styles.image} width='100%' src={value.link_background}/>
          <div className={styles.bottomCarou}>{value.name_vn}</div>
          <div className={styles.topLeft}>{value.ep_today}</div>
          <div className={styles.play}></div>
        </label>
      </Link>
    ));

    useEffect(() => {
        setPosition(window.innerWidth)
    }, [position]);
  return (
    <div className='col-12'>
      <div style={{textAlign:'left',padding:10,color:"#ff8a00",fontWeight:'bold',fontSize:16}}>PHIM ĐỀ CỬ</div>
    <Slider
        dots={false}
        slidesToShow={(position>1100)?5:(position>765?3:2)}
        slidesToScroll={2}
        autoplay={true}
        autoplaySpeed={3000}
      >
        {renderSlides()}
      </Slider>
    </div>
  );
}
export default UiCarousel