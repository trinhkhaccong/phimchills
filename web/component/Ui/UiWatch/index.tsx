import * as React from 'react';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { Player } from 'video-react';
import ReactHlsPlayer from 'react-hls-player';
import Link from "next/link"
import ShowMoreText from "react-show-more-text";

interface WatchInfoProp{
    name_vn:string;
    name_en:string;
    id_typephim:any;
    link_video_tm:string;
    link_video_sub:string;
    episode:number;
    content:string;
    link_background:string
  }

export interface UiWatchProps {
    episode:EpisodeProp[];
    datalink:WatchInfoProp;
}

interface EpisodeProp
{
  episode:number;
  root_link:string;
}
const UiWatch = (props: UiWatchProps) => {
    const {episode,datalink} =props
    const [position, setPosition] = useState(0);
    const router = useRouter()
    useEffect(() => {
        setPosition(window.innerWidth)
    }, [position]);

    React.useEffect(()=>{
        if(!router.isReady) return;

      },[router.isReady,props])
      
    
    return (
        <div >
            <div className='d-flex justify-content-between mt-3'>
            </div>
                <div>
                    <h1 className='title-film' >Phim {datalink?.name_vn} {datalink.id_typephim == 1?"":"- tập " +datalink?.episode}</h1>
                    { 
                        datalink?.link_video_sub.includes(".mp4?") && <Player 
                                    playsInline 
                                    poster={datalink?.link_background} 
                                    src={datalink?.link_video_sub}
                        />
                    }
                    {
                        datalink?.link_video_sub.includes(".m3u8") && <ReactHlsPlayer
                                        src={datalink?.link_video_sub}
                                        autoPlay={true}
                                        controls={true}
                                        width="100%"
                                        height="auto"
                        />    
                    } 

                    <div className='d-flex flex-wrap'>
                        <div  className='title-film'>Danh sách tập :</div>
                        <div style={{textAlign:'left'}}>{episode?.map(value=> (<Link href={"/watch/"+episode[0]?.root_link+"-tap-"+value.episode}><div style={{minWidth:40}} className={value.episode===props.datalink?.episode?"btn btn-primary btn-sm m-2":"btn btn-secondary btn-sm m-2"} >{value.episode}</div></Link>))}</div>                        
                    </div>
                    <div className='title-film' > Nội dung phim {props.datalink?.name_vn}</div>
                    <div style={{color:"#FFFFFF",textAlign:'left'}}>
                    <ShowMoreText
                                /* Default options */
                                lines={6}
                                more="xem thêm"
                                less="rút gọn"
                                className="content-css"
                                anchorClass="my-anchor-css-class"
                                expanded={false}
                                truncatedEndingComponent={" ... "}
                            >
                                 {props.datalink?.content}
                            </ShowMoreText>
                    </div>
            </div>
            <div id="container-971777154accb7bf0043aef787653bf6"></div>
        </div>
    );
}
export default UiWatch