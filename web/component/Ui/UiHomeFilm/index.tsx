import * as React from 'react';
import { useState, useEffect } from 'react';
import { Card } from 'antd';
import { useRouter } from 'next/router';
import styles from "../../../styles/Home.module.scss"
import Link from 'next/link';
const gridStyle1: React.CSSProperties = {
    width: "25%",
    textAlign: 'center',
    color: "#FFFFFF",
    backgroundColor: "#1a1a1a"
};

const gridStyle2: React.CSSProperties = {
    width: "33%",
    textAlign: 'center',
    color: "#FFFFFF",
    backgroundColor: "#1a1a1a"

};

const gridStyle3: React.CSSProperties = {
    width: "50%",
    textAlign: 'center',
    color: "#FFFFFF",
    backgroundColor: "#1a1a1a"

};
interface ObjectFilm {
    link: string;
    link_background: string;
    name_vn: string;
    ep_today:any;
}
export interface UiHomeFilmProps {
    title: any;
    url: any;
    data: ObjectFilm[]
}

const UiHomeFilm = (props: UiHomeFilmProps) => {
    const [position, setPosition] = useState(0);
    const router = useRouter()

    useEffect(() => {
        setPosition(window.innerWidth)
    }, [position]);
   
    return (
        <div >
            <div className='d-flex justify-content-between mt-3'>
                <div className='title-film' > {props.title}</div>
                <div className='title-detal'> <Link href={props.url} key={props.url}>{"xem tất cả >"}</Link> </div>
            </div>
            <Card>
                {
                    props.data?.map(value => (
                        <Card.Grid style={position > 1100 ? gridStyle1 : (position > 765 ? gridStyle2 : gridStyle3)} key={value.link + "-home-girl-film"}>
                            <Link href={"/info/" + value.link} key={value.link + "-home-film"}>
                                <div className={styles.home}>
                                    <img alt={value.name_vn} className={styles.imageHome} src={value.link_background} />
                                    <div className={styles.bottomHome}>{value.name_vn}</div>
                                    <div className={styles.topLeftHome}>{value.ep_today}</div>
                                    <div className={styles.play}></div>
                                </div>
                            </Link>
                        </Card.Grid>
                    ))
                }
            </Card>
            <div id="container-971777154accb7bf0043aef787653bf6"></div>
        </div>
    );
}
export default UiHomeFilm