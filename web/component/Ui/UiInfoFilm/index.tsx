import * as React from 'react';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import styles from "../../../styles/Info.module.scss"
import ShowMoreText from "react-show-more-text";
import Link from 'next/link';

export interface DataLinkProp {
    list_theloai: any;
    year: number;
    director: string;
    cast: string;
    content: string;
    link: string;
    link_background: string;
    name_vn: string;
    name_en: string;
    ep_today: string;
    time: string;
    country: string;
    theloai: string;
}

export interface UiInfoFilmProps {
    ret_link: DataLinkProp;
}

const UiInfoFilm = (props: UiInfoFilmProps) => {
    const [position, setPosition] = useState(0);
    const {
        list_theloai,
        director,
        cast,
        content,
        country,
        link,
        link_background,
        name_vn,
        name_en,
        ep_today,
        time,
        year
    } = props.ret_link

    useEffect(() => {
        setPosition(window.innerWidth)
    }, [position]);

    return (
        <div >
            <div className='d-flex justify-content-between mt-3'>
            </div>
            <div className='row'>
                <div className='col-md-6 col-xl-4 col-lg-4 p-3'>
                    <Link href={"/watch/"+link} key={"watch-"+link}>
                    <div className={styles.home}>
                        <img alt={name_vn +" - "+name_en} className={styles.imageHome} width='100%' height={"400px"} src={link_background} />
                        <div className={styles.bottomHome} >Xem phim</div>
                        <div className={styles.topLeftHome}>{ep_today}</div>
                        <div className={styles.play}></div>
                    </div>
                    </Link>
                </div>
                <div className='col-md-6 col-xl-8 col-lg-8 p-3' style={{ overflow: 'auto', textAlign: "left" }}>
                    <h1 style={{ color: "orangered" }} > {name_vn}</h1>
                    <div style={{ color: "#FFFFFF" }}>{name_en}</div>
                    <hr style={{ color: "#FFFFFF" }} />
                    <div className='d-flex'>
                        <h6 style={{ color: "#ff8a00" }} className='col-3'>Số tập:</h6>
                        <p style={{ color: "#FFFFFF" }}>{ep_today}</p>
                    </div>
                    <div className='d-flex'>
                        <h6 style={{ color: "#ff8a00" }} className='col-3'>Đạo diễn:</h6>
                        <p style={{ color: "#FFFFFF" }}>{director}</p>
                    </div>
                    <div className='d-flex'>
                        <h6 style={{ color: "#ff8a00" }} className='col-3'>Diễn viên:</h6>
                        <p style={{ color: "#FFFFFF" }}>{cast}</p>
                    </div>
                    <div className='d-flex'>
                        <h6 style={{ color: "#ff8a00" }} className='col-3'>Thể Loại:</h6>
                        <p style={{ color: "#FFFFFF" }}>{list_theloai}</p>
                    </div>
                    <div className='d-flex'>
                        <h6 style={{ color: "#ff8a00" }} className='col-3'>Quốc Gia:</h6>
                        <p style={{ color: "#FFFFFF" }}>{country}</p>
                    </div>
                    <div className='d-flex'>
                        <h6 style={{ color: "#ff8a00" }} className='col-3'>Năm sản xuất:</h6>
                        <p style={{ color: "#FFFFFF" }}>{year}</p>
                    </div>
                </div>
            </div>
			<div style={{textAlign:"left"}}>
                        <h6 style={{ color: "#ff8a00" }}>Nội dung phim {name_vn}</h6>
                        <p style={{ color: "#FFFFFF" }}>
                            <ShowMoreText
                                /* Default options */
                                lines={4}
                                more="xem thêm"
                                less="rút gọn"
                                className="content-css"
                                anchorClass="my-anchor-css-class"
                                expanded={false}
                                truncatedEndingComponent={" ... "}
                            >
                                {content}
                            </ShowMoreText>
                        </p>
                    </div>
<div id="container-971777154accb7bf0043aef787653bf6"></div>
        </div>
    );
}
export default UiInfoFilm