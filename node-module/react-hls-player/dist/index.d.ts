import React, { RefObject } from 'react';
import { Config } from 'hls.js';
export interface HlsPlayerProps extends React.VideoHTMLAttributes<HTMLVideoElement> {
    hlsConfig?: Config;
    src: string;
}
declare function ReactHlsPlayer({src, autoPlay, ...props }: HlsPlayerProps): JSX.Element;
export default ReactHlsPlayer;
