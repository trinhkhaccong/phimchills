const React= require('react');
const Hls = require('hls.js');
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};

const reactHlsPlayer=(_a)=> {
    var hlsConfig = _a.hlsConfig, _b = _a.playerRef, playerRef = _b === void 0 ? React.createRef() : _b, src = _a.src, autoPlay = _a.autoPlay, props = __rest(_a, ["hlsConfig", "playerRef", "src", "autoPlay"]);
    React.useEffect(function () {
        var hls;
        function _initPlayer() {
            if (hls != null) {
                hls.destroy();
            }
            var newHls = new Hls(__assign({ enableWorker: false }, hlsConfig));
            if (playerRef.current != null) {
                newHls.attachMedia(playerRef.current);
            }
            newHls.on(Hls.Events.MEDIA_ATTACHED, function () {
                newHls.loadSource(src);
                newHls.on(Hls.Events.MANIFEST_PARSED, function () {
                    var _a;
                    if (autoPlay) {
                        (_a = playerRef === null || playerRef === void 0 ? void 0 : playerRef.current) === null || _a === void 0 ? void 0 : _a.play().catch(function () {
                            return console.log('Unable to autoplay prior to user interaction with the dom.');
                        });
                    }
                });
            });
            newHls.on(Hls.Events.ERROR, function (event, data) {
                if (data.fatal) {
                    switch (data.type) {
                        case Hls.ErrorTypes.NETWORK_ERROR:
                            newHls.startLoad();
                            break;
                        case Hls.ErrorTypes.MEDIA_ERROR:
                            newHls.recoverMediaError();
                            break;
                        default:
                            _initPlayer();
                            break;
                    }
                }
            });
            hls = newHls;
        }
        _initPlayer();
        return function () {
            if (hls != null) {
                hls.destroy();
            }
        };
    }, [autoPlay, hlsConfig, playerRef, src]);
    return React.createElement("video", __assign({ ref: playerRef, src: src, autoPlay: autoPlay }, props));
}
module.exports = function ReactHlsPlayer(_a) {
    try {
      return reactHlsPlayer(_a);
    } catch (error) {
      if (((error.message || '').match(/stack|recursion/i))) {
        // warn on circular references, don't crash
        // browsers give this different errors name and messages:
        // chrome/safari: "RangeError", "Maximum call stack size exceeded"
        // firefox: "InternalError", too much recursion"
        // edge: "Error", "Out of stack space"
        console.warn('react-fast-compare cannot handle circular refs');
        return false;
      }
      // some other error. we should definitely know about these
      throw error;
    }
  };
  